#!/usr/bin/end php
<?php

use Actweb\Config;

/**
 * Determine the current run environment dev/prod/test
 *
 * Perform a series of tests to try and determine the current
 * environment that a script is running in.
 * Uses an INI file to define things like hostname and uname of
 * systems that are in development or testing, if nothing matches
 * then default to production to be safe.
 * Also checks for an environment var ENVIRONMENT
 * Priority is ENV/HOSTNAME/UNAME
 * ALL HOSTNAMES/UNAMES must be in lower case
 * INI file format :
 * test.example.com = test
 * dev.example.com = dev
 * prod.example.com = prod
 * User: matt
 * Date: 19/04/2020
 * Time: 16:00
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  ACTweb/<CategoryName>
 * @package   getEnviroment
 * @author    Matt Lowe <marl.scot.1@googlemail.com>
 * @copyright 2020 ACTweb
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   0.1.1
 * @link      http://www.actweb.info/package/getEnviroment
 */

/*
 * If called from command line then output the
 * diagnostics data, which just shows what each
 * test is set to.
 */

print_r(getEnvironment::diag());

class getEnvironment
{
    /**
     * @var array Of environments we will test for
     */
    static private $environments=array();
    /**
     * @var \Actweb\getEnvironment Instance
     */
    static private $instance;
    /**
     * @var string Path to the config ini file
     */
    static private $configPath='../config/environments.ini';
    /**
     * @var string Detected environment
     */
    static private $environment = null;
    /**
     * Basic Constructor
     */
    public function __construct()
    {
        $file=__DIR__.'/'.self::$configPath;
        self::$environments=parse_ini_file($file, true, INI_SCANNER_RAW);
    }

    /**
     * Ensure we have only a single instance of the class
     *
     * @return \Actweb\getEnvironment|\getEnvironment
     */
    private function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new getEnvironment();
        }
        return self::$instance;
    }

    public static function getEnvironment()
    {
        self::getInstance();
        if (null === self::$environment) {
            echo "Environment not set yet\n";
            self::detectEnvironment();
        }
        return self::$environment;
    }
    private static function detectEnvironment()
    {
        self::getInstance();
        if (getenv('ENVIRONMENT')) {
            $hostname = getenv('ENVIRONMENT');
        } elseif (isset($_ENV['HOSTNAME'])) {
            $hostname=strtolower($_ENV['HOSTNAME']);
        } else {
            $hostname=strtolower(php_uname("n"));
        }
        if (array_key_exists($hostname, self::$environments)) {
            self::$environment=self::$environments[$hostname];
        } else {
            self::$environment='prod';
        }
    }

    public static function diag()
    {
        $local=array(
            'Environment' => (getenv('ENVIRONMENT')) ? getenv('ENVIRONMENT') : 'NOT SET',
            'Hostname' => (isset($_ENV['HOSTNAME'])) ? $_ENV['HOSTNAME'] : 'NOT SET',
            'Uname' => php_uname("n"),
            'Calculated' => self::getEnvironment()
        );
        return $local;
    }
    /**
     * Returns true if we are in a dev environment
     *
     * @return bool True if in dev
     */
    public static function isDev()
    {
        return (self::getEnvironment() === 'dev');
    }
    /**
     * Returns true if we are in a test environment
     *
     * @return bool True if in test
     */
    public static function isTest()
    {
        return (self::getEnvironment() === 'test');
    }
    /**
     * Returns true if we are in a production environment
     *
     * @return bool True if in production
     */
    public static function isProd()
    {
        return (self::getEnvironment() === 'prod');
    }

}
